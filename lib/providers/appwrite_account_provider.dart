import 'package:appwrite/appwrite.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:todo_application/providers/appwrite_client.dart';

final appwriteAccountProvider = Provider((ref) => Account(appwriteClient));
