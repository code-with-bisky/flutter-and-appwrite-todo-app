// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:appwrite/appwrite.dart';
import 'package:appwrite/models.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:todo_application/providers/appwrite_account_provider.dart';

import 'package:todo_application/providers/appwrite_client.dart';

final accountRepositoryProvider =
    Provider((ref) => AccountRepositoryProvider(ref));

class AccountRepositoryProvider {
  Ref _ref;
  Account get _account => _ref.read(appwriteAccountProvider);

  AccountRepositoryProvider(this._ref);

  Future<User?> createNewAccount(
      String userId, String email, String password, String name) async {
    try {
      User user = await _account.create(
          userId: userId, email: email, password: password, name: name);

      await _account.createEmailSession(email: email, password: password);

      sendVerificationEmail();
      return user;
    } on AppwriteException catch (e) {
      print(e);
    }

    return null;
  }

  Future<String?> sendVerificationEmail() async {
    try {
      await _account.createVerification(
          url: 'https://verification.codewithbisky.com/verifyEmail');
      return null;
    } on AppwriteException catch (e) {
      print(e);
      return e.message;
    }
  }

  Future<User?> getCurrentUserFromSession() async {
    try {
      User user = await _account.get();
      return user;
    } on AppwriteException catch (e) {
      print(e);
      return null;
    } catch (e) {
      print(e);
     
    }
     return null;
  
  }

  Future<String?> login(String email, String password) async {
    try {
      await _account.createEmailSession(email: email, password: password);
      return null;
    } on AppwriteException catch (e) {
      print(e);

      return e.message;
    }
  }


  Future<String?> validateEmail(String userId, String secret) async {
    try {
      await _account.updateVerification(userId: userId, secret: secret);
      return null;
    } on AppwriteException catch (e) {
      print(e);

      return e.message;
    }
  }

  Future<void> googleSignIn() async{

    try {

      await _account.createOAuth2Session(provider: 'google');
      await Future.delayed(Duration(microseconds: 500));

    } on AppwriteException catch (e) {
      print(e);

    }


  }
}
