import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:todo_application/presentation/signup_screen/SignUpViewModel.dart';
import 'package:todo_application/providers/appwrite_account_repository_provider.dart';
import 'package:uni_links/uni_links.dart';
import 'core/app_export.dart';
import 'package:todo_application/theme/theme_helper.dart';
import 'package:todo_application/routes/app_routes.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  ///Please update theme as per your need if required.
  ThemeHelper().changeTheme('primary');
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends ConsumerStatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends ConsumerState<MyApp> {

  StreamSubscription? _sub;

  @override
  Widget build(BuildContext context) {


    _handleIncomingLinks();

    return MaterialApp.router(
        theme: theme,
        title: 'todo_application',
        debugShowCheckedModeBanner: false,
        localizationsDelegates: [
          AppLocalizationDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          Locale(
            'en',
            '',
          ),
        ],
        routerConfig: AppRoutes.router);
  }


  void _handleIncomingLinks() {


    if (!kIsWeb) {
      // It will handle app links while the app is already started - be it in
      // the foreground or in the background.
      _sub = uriLinkStream.listen((Uri? uri) async{

        print('got uri: $uri');


        if(uri != null){


          Map<String,String> params = uri.queryParameters;


          print('Parameters >>>> $params');
          if(params.containsKey('secret') && params.containsKey('userId')){

            String secret = params['secret']!;
            String userId = params['userId']!;

            print('UserId >>>> $userId');
            print('secret   >>>> $secret');

            await ref.read(accountRepositoryProvider) .validateEmail(userId, secret);



          }


        }





      }, onError: (Object err) {

        print(err);
      });
    }
  }


  
}
