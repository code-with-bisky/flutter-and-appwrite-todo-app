import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:todo_application/presentation/adacana_screen/adacana_screen.dart';
import 'package:todo_application/presentation/login_or_signup_screen/login_or_signup_screen.dart';
import 'package:todo_application/presentation/signup_screen/signup_screen.dart';
import 'package:todo_application/presentation/login_screen/login_screen.dart';
import 'package:todo_application/presentation/forgot_password_screen/forgot_password_screen.dart';
import 'package:todo_application/presentation/home_screen/home_screen.dart';
import 'package:todo_application/presentation/personality_screen/personality_screen.dart';
import 'package:todo_application/presentation/verify_email/verify_email_screen.dart';
import 'package:todo_application/presentation/work_today_screen/work_today_screen.dart';
import 'package:todo_application/presentation/wish_list_screen/wish_list_screen.dart';
import 'package:todo_application/presentation/settings_screen/settings_screen.dart';
import 'package:todo_application/presentation/language_screen/language_screen.dart';
import 'package:todo_application/presentation/termsandconditions_screen/termsandconditions_screen.dart';
import 'package:todo_application/presentation/app_navigation_screen/app_navigation_screen.dart';

class AppRoutes {
  static const String adacanaScreen = '/adacana_screen';

  static const String loginOrSignupScreen = '/login_or_signup_screen';

  static const String signupScreen = '/signup_screen';

  static const String loginScreen = '/login_screen';

  static const String forgotPasswordScreen = '/forgot_password_screen';

  static const String homeScreen = '/home_screen';

  static const String personalityScreen = '/personality_screen';

  static const String workTodayScreen = '/work_today_screen';

  static const String wishListScreen = '/wish_list_screen';

  static const String settingsScreen = '/settings_screen';

  static const String languageScreen = '/language_screen';

  static const String termsandconditionsScreen = '/termsandconditions_screen';

  static const String appNavigationScreen = '/app_navigation_screen';

    static const String verifyEmailScreen = '/verify_email_screen';

  static GoRouter router = GoRouter(
    routes: <RouteBase>[
      GoRoute(
        path: '/',
        builder: (BuildContext context, GoRouterState state) {
          return const AdacanaScreen();
        },
        routes: <RouteBase>[
          GoRoute(
            path: 'login_or_signup_screen',
            builder: (BuildContext context, GoRouterState state) {
              return const LoginOrSignupScreen();
            },
          ),
          GoRoute(
            path: 'signup_screen',
            builder: (BuildContext context, GoRouterState state) {
              return SignupScreen();
            },
          ),
          GoRoute(
            path: 'login_screen',
            builder: (BuildContext context, GoRouterState state) {
              return LoginScreen();
            },
          ),
          GoRoute(
            path: 'verify_email_screen',
            builder: (BuildContext context, GoRouterState state) {
              return VerifyEmailScreen();
            },
          ),
          GoRoute(
            path: 'home_screen',
            builder: (BuildContext context, GoRouterState state) {
              return HomeScreen();
            },
          )
        ],
      ),
    ],
  );

  static BuildContext? get ctx =>
      router.routerDelegate.navigatorKey.currentContext;
}
