import 'package:appwrite/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:todo_application/core/app_export.dart';
import 'package:todo_application/model/state/auth_state.dart';
import 'package:todo_application/presentation/signup_screen/SignUpViewModel.dart';
import 'package:todo_application/widgets/app_bar/appbar_iconbutton.dart';
import 'package:todo_application/widgets/app_bar/appbar_title.dart';
import 'package:todo_application/widgets/app_bar/custom_app_bar.dart';
import 'package:todo_application/widgets/custom_elevated_button.dart';
import 'package:todo_application/widgets/custom_text_form_field.dart';

// ignore_for_file: must_be_immutable
class LoginScreen extends ConsumerStatefulWidget {
  LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends ConsumerState<LoginScreen> {
  TextEditingController emailController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  SignupNotifier? _notifier;
  AuthState? _state;

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    _notifier = ref.read(signupNotifierProvider.notifier);
    _state = ref.watch(signupNotifierProvider);

    _notifier?.onMessage = (value) {
      print('message $value');
    };

    _notifier?.onEmailVerification = (value) {
      print('email verification $value');

      if (value == null) {
        print('navigate to dashboard');
         context.pushReplacement(AppRoutes.homeScreen);
      } else {
        print(' email verification error $value');
        context.pushReplacement(AppRoutes.verifyEmailScreen);
      }
    };

    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: CustomAppBar(
                leadingWidth: 56.h,
                leading: AppbarIconbutton(
                    svgPath: ImageConstant.imgArrowleft,
                    margin:
                        EdgeInsets.only(left: 32.h, top: 14.v, bottom: 17.v),
                    onTap: () {
                      onTapArrowleftone(context);
                    }),
                actions: [
                  AppbarTitle(
                      text: "lbl_login".tr,
                      margin: EdgeInsets.symmetric(
                          horizontal: 48.h, vertical: 14.v))
                ]),
            body: Form(
                key: _formKey,
                child: SingleChildScrollView(
                    padding: EdgeInsets.only(top: 11.v),
                    child: Padding(
                        padding: EdgeInsets.only(
                            left: 32.h, right: 32.h, bottom: 5.v),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  width: 221.h,
                                  margin: EdgeInsets.only(right: 89.h),
                                  child: Text("msg_this_is_your_community".tr,
                                      maxLines: 4,
                                      overflow: TextOverflow.ellipsis,
                                      style: theme.textTheme.displaySmall!
                                          .copyWith(height: 1.18))),
                              Container(
                                  width: 282.h,
                                  margin:
                                      EdgeInsets.only(top: 1.v, right: 28.h),
                                  child: Text("msg_our_community_is3".tr,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: theme.textTheme.labelLarge!
                                          .copyWith(height: 1.67))),
                              SizedBox(height: 21.v),
                              CustomImageView(
                                  svgPath: ImageConstant.imgLogoGray5001113x116,
                                  height: 113.v,
                                  width: 116.h,
                                  alignment: Alignment.center),
                              SizedBox(height: 115.v),
                              CustomTextFormField(
                                  controller: emailController,
                                  hintText: "lbl_email2".tr,
                                  textInputType: TextInputType.emailAddress,
                                  borderDecoration: TextFormFieldStyleHelper
                                      .outlineOnPrimaryTL14),
                              SizedBox(height: 24.v),
                              CustomTextFormField(
                                  controller: passwordController,
                                  hintText: "lbl_password".tr,
                                  textInputAction: TextInputAction.done,
                                  textInputType: TextInputType.visiblePassword,
                                  obscureText: true,
                                  borderDecoration: TextFormFieldStyleHelper
                                      .outlineOnPrimaryTL14),
                              SizedBox(height: 26.v),
                              GestureDetector(
                                  onTap: () {
                                    onTapTxtForgotpasswor(context);
                                  },
                                  child: Text("msg_forgot_password".tr,
                                      style: CustomTextStyles
                                          .labelLargeSecondaryContainerSemiBold)),
                              SizedBox(height: 23.v),
                              CustomElevatedButton(
                                  text: "lbl_next".tr,
                                  onTap: () {
                                    onTapNext(context);
                                  }),
                              SizedBox(height: 33.v),
                              Align(
                                alignment: AlignmentDirectional.center,
                                child: SizedBox(
                                  height: 50,
                                  child: ElevatedButton.icon(onPressed: () {
                                    _notifier?.signinWithGoogle();

                                  }, icon: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Image.asset('assets/images/google.png',width: 20,height: 20,),
                                  ),label: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text("sign_in_with_google".tr,style: TextStyle(color: Colors.white,fontSize: 13),),
                                  ),),
                                ),
                              ),
                              SizedBox(height: 33.v),
                              Align(
                                  alignment: Alignment.center,
                                  child: GestureDetector(
                                      onTap: () {
                                        onTapDonthaveanaccou(context);
                                      },
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    bottom: 1.v),
                                                child: Text(
                                                    "msg_don_t_have_an_account"
                                                        .tr,
                                                    style: theme
                                                        .textTheme.labelLarge)),
                                            Padding(
                                                padding:
                                                    EdgeInsets.only(left: 8.h),
                                                child: Text("lbl_signup2".tr,
                                                    style: CustomTextStyles
                                                        .labelLargeSecondaryContainerSemiBold))
                                          ])))
                            ]))))));
  }

  /// Navigates back to the previous screen.
  ///
  /// This function takes a [BuildContext] object as a parameter, which is used
  /// to navigate back to the previous screen.
  onTapArrowleftone(BuildContext context) {
    Navigator.pop(context);
  }

  /// Navigates to the forgotPasswordScreen when the action is triggered.
  ///
  /// The [BuildContext] parameter is used to build the navigation stack.
  /// When the action is triggered, this function uses the [Navigator] widget
  /// to push the named route for the forgotPasswordScreen.
  onTapTxtForgotpasswor(BuildContext context) {
    Navigator.pushNamed(context, AppRoutes.forgotPasswordScreen);
  }

  /// Navigates to the signupScreen when the action is triggered.
  ///
  /// The [BuildContext] parameter is used to build the navigation stack.
  /// When the action is triggered, this function uses the [Navigator] widget
  /// to push the named route for the signupScreen.
  onTapNext(BuildContext context) async {
    await _notifier?.login(emailController.text, passwordController.text);
  }

  /// Navigates to the signupScreen when the action is triggered.
  ///
  /// The [BuildContext] parameter is used to build the navigation stack.
  /// When the action is triggered, this function uses the [Navigator] widget
  /// to push the named route for the signupScreen.
  onTapDonthaveanaccou(BuildContext context) {
    Navigator.pushNamed(context, AppRoutes.signupScreen);
  }
}
