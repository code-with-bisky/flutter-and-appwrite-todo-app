import 'package:appwrite/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:todo_application/core/app_export.dart';
import 'package:todo_application/model/state/auth_state.dart';
import 'package:todo_application/presentation/signup_screen/SignUpViewModel.dart';

class AdacanaScreen extends ConsumerStatefulWidget {
  const AdacanaScreen({Key? key}) : super(key: key);

  @override
  _AdacanaScreenState createState() => _AdacanaScreenState();
}

class _AdacanaScreenState extends ConsumerState<AdacanaScreen> {
  SignupNotifier? _notifier;

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    _notifier = ref.read(signupNotifierProvider.notifier);

    _notifier?.onEmailVerification = (value) {
      if (value == null) {
        print(' Email was verified');
        // todo navigate to home page
        context.pushReplacement(AppRoutes.homeScreen);
      } else {
        print(value);

        onTapImgLogoone();
      }
    };

    return SafeArea(
        child: Scaffold(
            body: SizedBox(
                width: double.maxFinite,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: 1.v),
                      CustomImageView(
                          imagePath: ImageConstant.imgLogo,
                          height: 325.v,
                          width: 238.h,
                         )
                    ]))));
  }

  /// Navigates to the loginOrSignupScreen when the action is triggered.
  ///
  /// The [BuildContext] parameter is used to build the navigation stack.
  /// When the action is triggered, this function uses the [Navigator] widget
  /// to push the named route for the loginOrSignupScreen.
  onTapImgLogoone() {
    context.pushReplacement(AppRoutes.loginOrSignupScreen);
  }

    @override
  void initState() {
    super.initState();

 Future<void>.delayed(const Duration(seconds: 3));

     WidgetsBinding.instance
      .addPostFrameCallback((_) => initialization());
 
  }

  initialization() async {
    try {
      User? user = await _notifier?.getUser();

      if (user == null) {
        onTapImgLogoone();
      } else {
        if (user.emailVerification == true) {
          context.pushReplacement(AppRoutes.homeScreen);
        } else {
          context.pushReplacement(AppRoutes.verifyEmailScreen);
        }
      }
    } catch (e) {
      print(e);
       onTapImgLogoone();
    }
  }
}
