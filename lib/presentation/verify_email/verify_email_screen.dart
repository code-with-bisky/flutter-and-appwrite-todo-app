import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:todo_application/core/app_export.dart';
import 'package:todo_application/model/state/auth_state.dart';
import 'package:todo_application/presentation/signup_screen/SignUpViewModel.dart';
import 'package:todo_application/widgets/app_bar/appbar_iconbutton.dart';
import 'package:todo_application/widgets/app_bar/appbar_title.dart';
import 'package:todo_application/widgets/app_bar/custom_app_bar.dart';
import 'package:todo_application/widgets/custom_elevated_button.dart';
import 'package:todo_application/widgets/custom_text_form_field.dart';

// ignore_for_file: must_be_immutable
class VerifyEmailScreen extends ConsumerStatefulWidget {
  VerifyEmailScreen({Key? key}) : super(key: key);

  @override
  _VerifyEmailScreenState createState() => _VerifyEmailScreenState();
}

class _VerifyEmailScreenState extends ConsumerState<VerifyEmailScreen> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  SignupNotifier? _notifier;
  AuthState? _state;

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    _notifier = ref.read(signupNotifierProvider.notifier);
    _state = ref.watch(signupNotifierProvider);

    _notifier?.onResendEmail = (value) {
      if (value == null) {
        print(' Email was sent');
        // todo show snackbar success
      } else {
        print(value);
        // todo show snackbar error
      }
    };

    _notifier?.onEmailVerification = (value) {
      if (value == null) {
        print(' Email was verified');
        // todo navigate to home page
        context.pushReplacement(AppRoutes.homeScreen);
      } else {
        print(value);
        // todo show snackbar error
      }
    };

    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: CustomAppBar(
                leadingWidth: 56.h,
                leading: AppbarIconbutton(
                    svgPath: ImageConstant.imgArrowleft,
                    margin:
                        EdgeInsets.only(left: 32.h, top: 14.v, bottom: 17.v),
                    onTap: () {
                      onTapArrowleftone(context);
                    }),
                actions: [
                  AppbarTitle(
                      text: "lbl_verify_email".tr,
                      margin: EdgeInsets.symmetric(
                          horizontal: 35.h, vertical: 14.v))
                ]),
            body: Form(
                key: _formKey,
                child: Container(
                    width: double.maxFinite,
                    padding:
                        EdgeInsets.symmetric(horizontal: 31.h, vertical: 22.v),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                              height: 183.v,
                              width: 262.h,
                              child: Stack(
                                  alignment: Alignment.topCenter,
                                  children: [
                                    Align(
                                        alignment: Alignment.bottomLeft,
                                        child: SizedBox(
                                            width: 245.h,
                                            child: Text(
                                                "msg_if_you_are_always".tr,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                style: theme
                                                    .textTheme.labelLarge!
                                                    .copyWith(height: 1.67)))),
                                    Align(
                                        alignment: Alignment.topCenter,
                                        child: SizedBox(
                                            width: 262.h,
                                            child: Text(
                                                "msg_helping_others_means".tr,
                                                maxLines: 3,
                                                overflow: TextOverflow.ellipsis,
                                                style: theme
                                                    .textTheme.displaySmall!
                                                    .copyWith(height: 1.29))))
                                  ])),
                          SizedBox(height: 37.v),
                          CustomImageView(
                              svgPath: ImageConstant.imgLogoGray5001121x135,
                              height: 121.v,
                              width: 135.h,
                              alignment: Alignment.center),
                          SizedBox(height: 27.v),
                          Align(
                              alignment: Alignment.center,
                              child: Text("lbl_verify_email".tr,
                                  style: theme.textTheme.titleMedium)),
                          SizedBox(height: 45.v),
                          Align(
                            alignment: AlignmentDirectional.center,
                            child: Icon(
                              Icons.email,
                              size: 50,
                            ),
                          ),
                          CustomElevatedButton(
                              text: "lbl_resend_link".tr,
                              margin: EdgeInsets.only(),
                              onTap: () {
                                _notifier?.resendEmail();
                              })
                        ]))),
            bottomNavigationBar: CustomElevatedButton(
                text: "lbl_done".tr,
                margin: EdgeInsets.only(left: 32.h, right: 32.h, bottom: 52.v),
                onTap: () {
                  _notifier?.verifyUser();
                })));
  }

  /// Navigates back to the previous screen.
  ///
  /// This function takes a [BuildContext] object as a parameter, which is used
  /// to navigate back to the previous screen.
  onTapArrowleftone(BuildContext context) {
    Navigator.pop(context);
  }

  onTapStart(BuildContext context) {}
}
