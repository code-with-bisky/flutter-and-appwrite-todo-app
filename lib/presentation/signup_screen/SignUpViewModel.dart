import 'package:appwrite/models.dart';
import 'package:flutter/material.dart';
import 'package:objectid/objectid.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:todo_application/model/state/auth_state.dart';
import 'package:todo_application/providers/appwrite_account_repository_provider.dart';
part 'SignUpViewModel.g.dart';

@riverpod
class SignupNotifier extends _$SignupNotifier {
  AccountRepositoryProvider get _accountRepositoryProvider =>
      ref.read(accountRepositoryProvider);

  ValueChanged<String?>? onAccountCreation;
  ValueChanged<String?>? onResendEmail;
  ValueChanged<String?>? onMessage;
  ValueChanged<String?>? onEmailVerification;
  @override
  AuthState build() {
    return AuthState();
  }

  setEmail(String input) {
    state = state.copyWith(email: input);
  }

  setFirstName(String input) {
    state = state.copyWith(firstName: input);
  }

  setLastName(String input) {
    state = state.copyWith(lastName: input);
  }

  setPassword(String input) {
    state = state.copyWith(password: input);
  }

  loader(bool input) {
    state = state.copyWith(loading: input);
  }

  void createAccount() async {
    loader(true);
    User? user = await _accountRepositoryProvider.createNewAccount(
        ObjectId().hexString,
        state.email ?? "",
        state.password ?? "",
        '${state.firstName ?? ""} ${state.lastName ?? ""}');
    loader(false);
    if (user == null) {
// todo failed to create user

      onAccountCreation!("Failed to create an account, please try again later");
      return;
    } else {
      print('account was created');

      onAccountCreation!(null);
    }
  }

  void resendEmail() async {
    String? message = await _accountRepositoryProvider.sendVerificationEmail();

    onResendEmail!(message);
  }

  void verifyUser() async {
    User? user = await getUser() ;

    if (user != null) {
      if (user.emailVerification == true) {
        onEmailVerification!(null);
        return;
      }
    }

    onEmailVerification!('Please verify your email');
  }



 Future<User?> getUser() async {

    return await _accountRepositoryProvider.getCurrentUserFromSession();
  }


  login(String email, String password) async {
    String? message = await _accountRepositoryProvider.login(email, password);

    if (message == null) {
      User? user = await _accountRepositoryProvider.getCurrentUserFromSession();

      if (user != null) {
        if (user.emailVerification == true) {
          onEmailVerification!(null);
          return;
        } else {
          onEmailVerification!('Please verify your email');
        }
      }else{
         onMessage!('Please try to login again');
      }
    } else {
      onMessage!(message);
    }
  }

  void signinWithGoogle() async{

     await _accountRepositoryProvider.googleSignIn();

      User? user = await _accountRepositoryProvider.getCurrentUserFromSession();

      if (user != null) {
        onEmailVerification!(null);
      }else{
        onMessage!('Failed to login, please try again later');
      }
    }


}
