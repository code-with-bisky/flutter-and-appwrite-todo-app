// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SignUpViewModel.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$signupNotifierHash() => r'40a997527c3e233431f3b638c804914df1058ace';

/// See also [SignupNotifier].
@ProviderFor(SignupNotifier)
final signupNotifierProvider =
    AutoDisposeNotifierProvider<SignupNotifier, AuthState>.internal(
  SignupNotifier.new,
  name: r'signupNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$signupNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SignupNotifier = AutoDisposeNotifier<AuthState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
