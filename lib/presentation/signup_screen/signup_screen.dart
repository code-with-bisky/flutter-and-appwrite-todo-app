import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:todo_application/core/app_export.dart';
import 'package:todo_application/model/state/auth_state.dart';
import 'package:todo_application/presentation/signup_screen/SignUpViewModel.dart';
import 'package:todo_application/widgets/app_bar/appbar_iconbutton.dart';
import 'package:todo_application/widgets/app_bar/appbar_title.dart';
import 'package:todo_application/widgets/app_bar/custom_app_bar.dart';
import 'package:todo_application/widgets/custom_elevated_button.dart';
import 'package:todo_application/widgets/custom_text_form_field.dart';

// ignore_for_file: must_be_immutable
class SignupScreen extends ConsumerStatefulWidget {
  SignupScreen({Key? key}) : super(key: key);

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends ConsumerState<SignupScreen> {
  TextEditingController firstNameController = TextEditingController();

  TextEditingController lastNameController = TextEditingController();

  TextEditingController emailController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  SignupNotifier? _notifier;
  AuthState? _state;

  @override
  Widget build(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);

    _notifier = ref.read(signupNotifierProvider.notifier);
    _state = ref.watch(signupNotifierProvider);

    _notifier?.onAccountCreation = (value) {
      if (value != null) {
        //todo show error message
      } else {
        context.pushReplacement(AppRoutes.verifyEmailScreen);
      }
    };

    return SafeArea(
        child: Scaffold(
            backgroundColor: appTheme.gray5001,
            resizeToAvoidBottomInset: false,
            appBar: CustomAppBar(
                leadingWidth: 52.h,
                leading: AppbarIconbutton(
                    svgPath: ImageConstant.imgArrowleft,
                    margin:
                        EdgeInsets.only(left: 28.h, top: 14.v, bottom: 17.v),
                    onTap: () {
                      onTapArrowleftone(context);
                    }),
                actions: [
                  AppbarTitle(
                      text: "lbl_sign_up".tr,
                      margin: EdgeInsets.symmetric(
                          horizontal: 45.h, vertical: 14.v))
                ]),
            body: Form(
                key: _formKey,
                child: SingleChildScrollView(
                    padding: EdgeInsets.only(top: 27.v),
                    child: Padding(
                        padding: EdgeInsets.only(
                            left: 28.h, right: 32.h, bottom: 5.v),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                  width: 218.h,
                                  child: Text("msg_when_community_comes".tr,
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis,
                                      style: theme.textTheme.displaySmall!
                                          .copyWith(height: 1.18))),
                              Container(
                                  width: 245.h,
                                  margin: EdgeInsets.only(right: 69.h),
                                  child: Text("msg_our_community_is2".tr,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: CustomTextStyles.bodySmallBlack900
                                          .copyWith(height: 1.67))),
                              SizedBox(height: 12.v),
                              CustomImageView(
                                  svgPath: ImageConstant.imgLogoGray5001120x114,
                                  height: 120.v,
                                  width: 114.h,
                                  alignment: Alignment.center),
                              SizedBox(height: 96.v),
                              CustomTextFormField(
                                  controller: firstNameController,
                                  hintText: "lbl_first_name".tr),
                              SizedBox(height: 24.v),
                              CustomTextFormField(
                                  controller: lastNameController,
                                  hintText: "lbl_last_name".tr),
                              SizedBox(height: 24.v),
                              CustomTextFormField(
                                  controller: emailController,
                                  hintText: "lbl_e_mail".tr,
                                  textInputType: TextInputType.emailAddress),
                              SizedBox(height: 24.v),
                              CustomTextFormField(
                                  controller: passwordController,
                                  hintText: "lbl_password".tr,
                                  textInputAction: TextInputAction.done,
                                  textInputType: TextInputType.visiblePassword,
                                  obscureText: true),
                              Container(
                                  width: 248.h,
                                  margin:
                                      EdgeInsets.only(top: 22.v, right: 65.h),
                                  child: Text("msg_lorem_ipsum_dolor".tr,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: theme.textTheme.labelLarge!
                                          .copyWith(height: 1.67))),
                              CustomElevatedButton(
                                text: "lbl_next".tr,
                                margin: EdgeInsets.only(left: 4.h, top: 30.v),
                                onTap: () {
                                  // todo validation

                                  _notifier?.setEmail(emailController.text);
                                  _notifier
                                      ?.setFirstName(firstNameController.text);
                                  _notifier
                                      ?.setLastName(lastNameController.text);
                                  _notifier
                                      ?.setPassword(passwordController.text);
                                  _notifier?.createAccount();
                                },
                              ),
                              SizedBox(height: 29.v),
                              Align(
                                  alignment: Alignment.center,
                                  child: GestureDetector(
                                      onTap: () {
                                        onTapAlreadyhavean(context);
                                      },
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text("msg_already_have_an".tr,
                                                style:
                                                    theme.textTheme.labelLarge),
                                            Padding(
                                                padding:
                                                    EdgeInsets.only(left: 8.h),
                                                child: Text("lbl_login".tr,
                                                    style: CustomTextStyles
                                                        .labelLargeSecondaryContainerSemiBold))
                                          ])))
                            ]))))));
  }

  /// Navigates back to the previous screen.
  ///
  /// This function takes a [BuildContext] object as a parameter, which is used
  /// to navigate back to the previous screen.
  onTapArrowleftone(BuildContext context) {
    Navigator.pop(context);
  }

  /// Navigates to the loginScreen when the action is triggered.
  ///
  /// The [BuildContext] parameter is used to build the navigation stack.
  /// When the action is triggered, this function uses the [Navigator] widget
  /// to push the named route for the loginScreen.
  onTapAlreadyhavean(BuildContext context) {
    Navigator.pushNamed(context, AppRoutes.loginScreen);
  }
}
