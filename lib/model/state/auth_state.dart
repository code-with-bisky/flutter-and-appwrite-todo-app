// ignore_for_file: public_member_api_docs, sort_constructors_first
class AuthState {
  bool? loading;
  String? firstName;
  String? lastName;
  String? email;
  String? password;
  AuthState({
    this.loading,
    this.firstName,
    this.lastName,
    this.email,
    this.password,
  });

  AuthState copyWith({
    bool? loading,
    String? firstName,
    String? lastName,
    String? email,
    String? password,
  }) {
    return AuthState(
      loading: loading ?? this.loading,
      firstName: firstName ?? this.firstName,
      lastName: lastName ?? this.lastName,
      email: email ?? this.email,
      password: password ?? this.password,
    );
  }
}
