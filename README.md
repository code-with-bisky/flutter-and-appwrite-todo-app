
# todo_application
### Table of contents
- [System requirements](#system-requirements)
- [Figma design guidelines for better UI accuracy](#figma-design-guideline-for-better-accuracy)
- [Check the UI of the entire app](#app-navigations)
- [Application structure](#project-structure)
- [How to format your code?](#how-you-can-do-code-formatting)
- [How you can improve code readability?](#how-you-can-improve-the-readability-of-code)
- [Libraries and tools used](#libraries-and-tools-used)
- [Support](#support)

### System requirements

Dart SDK Version 2.18.0 or greater.
Flutter SDK Version 3.3.0 or greater.

### Figma design guidelines for better UI accuracy

Read our guidelines to increase the accuracy of design-to-code conversion by optimizing Figma designs.
https://docs.dhiwise.com/docs/Designguidelines/intro

### Check the UI of the entire app

Check the UI of all the app screens from a single place by setting up the 'initialRoute'  to AppNavigation in the AppRoutes.dart file.

### Application structure

After successful build, your application structure should look like this:

```
.
├── android                         - It contains files required to run the application on an Android platform.
├── assets                          - It contains all images and fonts of your application.
├── ios                             - It contains files required to run the application on an iOS platform.
├── lib                             - Most important folder in the application, used to write most of the Dart code..
    ├── main.dart                   - Starting point of the application
    ├── core
    │   ├── app_export.dart         - It contains commonly used file imports
    │   ├── constants               - It contains static constant class file
    │   └── utils                   - It contains common files and utilities of the application
    ├── presentation                - It contains widgets of the screens 
    ├── routes                      - It contains all the routes of the application
    └── theme                       - It contains app theme and decoration classes
    └── widgets                     - It contains all custom widget classes
```

### How to format your code?

- if your code is not formatted then run following command in your terminal to format code
  ```
  dart format .
  ```

### How you can improve code readability?

Resolve the errors and warnings that are shown in the application.

### Libraries and tools used

- Riverpod - State management
  https://riverpod.dev/docs/getting_started
- cached_network_image - For storing internet image into cache
  https://pub.dev/packages/cached_network_image

### Support

If you have any problems or questions, go to our Discord channel, where we will help you as quickly as possible: https://discord.com/invite/rFMnCG5MZ7


arch -x86_64 pod install
flutter packages pub run build_runner build



https://developer.android.com/training/app-links
https://developer.android.com/training/app-links#android-app-links

adb shell pm get-app-links com.todoapplication.app
adb shell pm get-app-links --user cur com.todoapplication.app
adb shell pm verify-app-links --re-verify com.todoapplication.app



[{
  "relation": ["delegate_permission/common.handle_all_urls"],
  "target": {
    "namespace": "android_app",
    "package_name": "com.todoapplication.app",
    "sha256_cert_fingerprints":
    ["8C:FC:A5:A8:DE:60:B0:B0:69:20:66:C3:2C:74:0A:12:F3:F6:AB:CC:91:B5:F2:F6:87:69:65:59:F2:96:E0:BF"]
  }
}]


adb shell 'am start -a android.intent.action.VIEW \
    -c android.intent.category.BROWSABLE \
    -d "https://verification.codewithbisky.com/verifyEmail?userId=65135ad1005e6c97ece7dc1d&secret=1a5ffdce3137a6d351cfe377f581c315f55b235062f0f1636f2dde3fadfbc009ee86434a8763500c1b40fdb2a892836b884514e9175125a0e5b5cda4c87ea405de4b294d425772353c133714ad8eb8ca48edc998ec1a6b54b2f09928d3e34d9f2ae28dae72fe0a81d34f334952e148ae1f73d1e7f32d201a5e57cf2ddec073b9&expire=2023-10-03+22%3A28%3A14.678"' \
    com.todoapplication.app
